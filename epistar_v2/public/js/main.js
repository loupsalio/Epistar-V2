const login_return = "<div class=\"input-section email-section\">\n" +
    "                    <input id=\"login\" type = \"text\" name = \"login\" placeholder=\"Epitech Email\" autocomplete=\"off\"/>\n" +
    "                </div>\n" +
    "                <input id=\"password\" type = \"password\" name = \"password\" placeholder=\"Blih Password\"/>\n" +
    "                <button id=\"connexion_button\">Connexion</button>";

function get_return(name){
    return " <div id=\"connect_info\">\n" +
        "                    <p id=\"connect_info_text\">Connecté en tant que <span id=\"connect_name\"> clemen_l\n" +
        "</span> </p>\n" +
        "                </div>\n" +
        "                <button id=\"logout_button\">Deconnexion</button>";
}

const small_size = 0.4;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {

    if($('#connect_name').size()){
        const new_width = $("#logo_star").width() * small_size;
        const next_width = new_width.toString() + 'px';
        $("#logo_star").css("width", next_width);
        $("#logo_star").css("top", "100px");
        $('.content').css("display", "absolute");
        $('.nav').show();
        $('.content').show();
        $('#log_div').css("bottom", 0);
    }


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('body').on('click', '#connexion_button', function(){
        const login = $('#login').val();
        const passwd = $('#password').val();

        if (login !== '' && passwd !== ''){
            $.ajax({
                type: 'POST',
                url: "/login",
                data: "login=" + login  + "&password=" + passwd,
                error: function (request, error) {
                    console.log("Erreur : responseText: " + request.responseText);
                },
                success: function (data) {
                    const new_width = $("#logo_star").width() * small_size;
                    const next_width = new_width.toString() + 'px';
                    if(data != -1){
                       $('.nav').show(1000);
                       $( "#logo_star" ).animate({
                           width:  next_width,
                           top: "100px"
                       }, 1000, function(){
                       });
//                        $( "#log_div" ).css({"transform":"translate(0px,0px)"}, 2000);
                        $( "#log_div" ).animate({
                            bottom: 0,
                        }, 1000);
                        $( ".content" ).show(1000);

                        $('#log_div').html(get_return(data));

                   }
                   else{
                       //TODO
                   }

                }
            });
        }

    });


    $('body').on('click', '#logout_button', function(){

        $.ajax({
            type: 'POST',
            url: "/logout",
            error: function (request, error) {
                console.log("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                const new_width = $("#logo_star").width() * (1 / small_size);
                const next_width = new_width.toString() + 'px';
                console.log(next_width);
                $('.nav').hide(1000);
                $( "#logo_star" ).animate({
                    width:  next_width,
                    top: "35%"
                }, 1000, function(){
                });
                $( "#log_div" ).animate({
                    bottom: "2%",
                }, 1000);
                $( ".content" ).hide(1000);
                $('#log_div').html(login_return);
                $('.content').html(data);
            }
        });
    });

    $('body').on('click', '#nav-accueil', function(){

        $.ajax({
            type: 'POST',
            url: "/accueil",
            error: function (request, error) {
                console.log("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                $('.content').html(data);
            }
        });
    });

    $('body').on('click', '#nav-infos', function(){

        $.ajax({
            type: 'POST',
            url: "/informations",
            error: function (request, error) {
                console.log("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                $('.content').html(data);
            }
        });
    });

    $('body').on('click', '#nav-ssh-keys', function(){

        $.ajax({
            type: 'POST',
            url: "/keys",
            error: function (request, error) {
                console.log("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                $('.content').html(data);
            }
        });
    });

    $('body').on('click', '#nav-repos', function(){

        $.ajax({
            type: 'POST',
            url: "/repos",
            error: function (request, error) {
                console.log("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                $('.content').html(data);
            }
        });
    });

});