<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Vinkla\Alert\Facades\Alert;

class mainController extends Controller
{
    static public $whitelist_status = 2;


    protected function run($command)
    {
        $envopts = array();
        $descriptorspec = array(
            1 => array('pipe', 'w'),
            2 => array('pipe', 'w'),
        );
        $pipes = array();

        if (count($_ENV) === 0) {
            $env = NULL;
            foreach ($envopts as $k => $v) {
                putenv(sprintf("%s=%s", $k, $v));
            }
        } else {
            $env = array_merge($_ENV, $envopts);
        }
        $cwd = ".";
        $resource = proc_open($command, $descriptorspec, $pipes, $cwd, $env);
        $stdout = stream_get_contents($pipes[1]);
        $stderr = stream_get_contents($pipes[2]);
        foreach ($pipes as $pipe) {
            fclose($pipe);
        }
        $status = trim(proc_close($resource));

        return $stdout;
    }

    public function get_repos_list()
    {
        $cmd = "python3 storage/blih.py -u " . Session::get('mail') . " -t \"" . Crypt::decryptString(Session::get('password')) . "\" repository list";
        $str = $this->run($cmd);
        $t = strpos($str, "Error");
        if ($t != false) {
            return null;
        }
        $res = explode("\n", $str);
        asort($res);
        array_shift($res);
        if (empty($res))
            $res = "No repos";
        return $res;
    }

    public function get_keys_list()
    {
        $list = array();

        $cmd = "python3 storage/blih.py -u " . Session::get('mail') . " -t \"" . Crypt::decryptString(Session::get('password')) . "\" sshkey list";
        $str = $this->run($cmd);
        $t = strpos($str, "Error");
        if ($t != false) {
            return null;
        }
        $res = explode("\n", $str);
        array_pop($res);
        foreach ($res as $t) {
            $elem = explode(" ", $t);
            array_push($list, $elem[2]);
        }
        asort($list);
        if (empty($list))
            $res = "No repos";
        return $list;
    }

    public function index()
    {
//        var_dump(Auth::check());
        return view('welcome', array('wl_status' => $this::$whitelist_status));
    }

    public function login()
    {
        $mail = Input::get('login');
        $passwd = Crypt::encryptString(addslashes(Input::get('password')));
        $str = $this->run("python3 storage/blih.py -u " . $mail . " -t \"" . Crypt::decryptString($passwd) . "\" whoami");
        $t = strpos($str, "Error 401");
        if ($t != false) {
            return -1;
        }
        Session::put('name', $str);
        Session::put('mail', $mail);
        Session::put('password', $passwd);
        Auth::attempt(['email' => "admin@admin.fr", 'password' => "adminadmin"], true);
        return $str;
    }

    public function logout()
    {
        Session::forget("name");
        Session::forget("mail");
        Session::forget("password");
        Auth::logout();
        return view('accueil', array('wl_status' => $this::$whitelist_status));
    }

    public function accueil()
    {
        return view('accueil', array('wl_status' => $this::$whitelist_status));
    }

    public function informations()
    {
        return view('informations');
    }

    public function repos()
    {
        if (Auth::check()) {
            $list = $this->get_repos_list();
            return view('repos', array('list' => $list));
        } else
            return view("not_log");
    }

    public function keys()
    {
        if (Auth::check()) {
            $list = $this->get_keys_list();
            return view('keys', array('list' => $list));
        } else
            return view("not_log");

    }
}
