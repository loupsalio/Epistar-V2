<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Main pages */

Route::get('/', "mainController@index")->name("home");
Route::post('/accueil', "mainController@accueil")->name("accueil");
Route::post('/informations', "mainController@informations")->name("informations");
Route::post('/repos', "mainController@repos")->name("repos");
Route::post('/keys', "mainController@keys")->name("keys");

/* Login */

Route::post('/login', "mainController@login")->name("login");
Route::post('/logout', "mainController@logout")->name("logout");
//Auth::routes();
