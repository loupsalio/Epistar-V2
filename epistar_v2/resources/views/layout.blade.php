<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Epistar</title>

    <!-- Links -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/png" href="{{ asset('storage/Epistar-V2.png') }}"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src={{ asset('js/main.js') }}></script>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

</head>

<header>
    <a href="{{ route('home') }}"><img id="logo_star" src="{{ asset('storage/Epistar-V2.png') }}" ></a>
    <h1>Epistar-V2-Prod</h1>
    <nav>
        <div id="nav-left">
            <h3 id="nav-accueil" class="nav">
                <p>Accueil</p>
            </h3>
            <h3 id="nav-infos" class="nav">
                <p>Informations</p>
            </h3>
        </div>
        <div id="nav-right">
            <h3 id="nav-ssh-keys" class="nav">
                <p>SSH-keys</p>
            </h3>
            <h3 id="nav-repos" class="nav">
                <p>Repositories</p>
            </h3>
        </div>
    </nav>
    <div id="filtre"></div>
</header>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div id="log_div" class="top-right links">
            @auth
                <div id="connect_info">
                    <p id="connect_info_text">Connecté en tant que <span id="connect_name"> {{ Session::get("name") }}</span> </p>
                </div>
                <button id="logout_button">Deconnexion</button>
            @else
                <div class="input-section email-section">
                    <input id="login" type = "text" name = "login" placeholder="Epitech Email" autocomplete="off"/>
                </div>
                <input id="password" type = "password" name = "password" placeholder="Blih Password"/>
                <button id="connexion_button">connexion</button>
            @endauth
        </div>
    @endif
    @include('alert::bootstrap')
    <div class="content">
        @yield('content')
    </div>
</div>
</body>
</html>
