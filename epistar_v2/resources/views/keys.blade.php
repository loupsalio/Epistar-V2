<?php
/**
 * User: wilk
 * Date: 08/08/18
 * Time: 09:32
 */
?>

<div id="repos">

    @foreach($list as $key)
    <div class="key">
        <p class="key_name">{{ $key }}</p>
        <div class="key_actions">
            <button class="key_action_delete">Delete</button>
        </div>
    </div>
    @endforeach

</div>
