<?php
/**
 * User: wilk
 * Date: 08/08/18
 * Time: 08:44
 */
?>
<div id="core"><h2 class="w3-text-light-grey">Informations</h2>
    <hr style="width:200px" class="w3-opacity">
    <p>EpiStar est une interface web utilisant <a href="https://github.com/bocal/blih">BLIH</a>.
        <br/> EpiStar n'est en aucun cas responsable de vos repos, utilisez les commandes terminales si vous souhaitez une fiabilité "absolue". </p>
    <p>EpiStar n'utilisera ou partagera jamais vos informations.</p>
</div>
