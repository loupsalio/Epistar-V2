<?php
/**
 * User: wilk
 * Date: 08/08/18
 * Time: 09:32
 */
?>

<div id="repos">

    @foreach($list as $repo)
    <div class="repo">
        <p class="repo_name">{{ $repo }}</p>
        <div class="repo_actions">
            <button class="repo_action_acls">ACL</button>
            <button class="repo_action_delete">Delete</button>
        </div>
        <div class="repo_acls"></div>
    </div>
    @endforeach

</div>
